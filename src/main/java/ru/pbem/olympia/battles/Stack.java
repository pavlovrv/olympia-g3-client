package ru.pbem.olympia.battles;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Роман
 * Date: 06.06.2015
 */
public class Stack {
    protected Unit leader;
    protected Unit shelter;
    protected List<Squad> followers;
    protected List<Stack> stacks;

    protected Stack() {
        this.leader = null;
        this.shelter = null;
        this.followers = new ArrayList<Squad>();
        this.stacks = new ArrayList<Stack>();
    }

    public Unit getLeader() {
        return leader;
    }

    public Unit getShelter() {
        return shelter;
    }

    public List<Squad> getFollowers() {
        return followers;
    }

    public List<Stack> getStacks() {
        return stacks;
    }

    public static class Squad {
        public int num;
        public Unit unit;

        public Squad(int num, Unit template) {
            this.num = num;
            this.unit = template;
        }

        @Override
        public String toString() {
            return num + " " + unit;
        }
    }

    @Override
    public String toString() {
        return doToString("", "\t");
    }

    public String doToString(String indent, String base) {
        String str = indent + getLeader().toString();
        if (!followers.isEmpty()) {
            str += " with followers: " + followers.stream().map(Squad::toString).reduce((s1, s2) -> s1 + ", " + s2).orElse("");
        }
        if (!stacks.isEmpty()) {
            str += "\n" + indent + stacks.stream().map(s->s.doToString(indent + base, base)).reduce((s1, s2) -> s1 + "\n\t" + s2).orElse("");
        }
        return str;
    }

}
