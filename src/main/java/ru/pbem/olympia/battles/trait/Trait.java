package ru.pbem.olympia.battles.trait;

import java.util.function.Function;
import java.util.function.Predicate;

/**
 * User: Роман
 * Date: 06.06.2015
 */
public class Trait {
    protected Type type;
    protected Scope scope;
    protected Predicate prerequisite;
    protected Function effect;

    public Trait(Type type, Scope scope, Function effect) {
        this(type, scope, null, effect);
    }

    public Trait(Type type, Scope scope, Predicate prerequisite, Function effect) {
        this.type = type;
        this.scope = scope;
        this.prerequisite = prerequisite;
        this.effect = effect;
    }

    public Type getType() {
        return type;
    }

    public Scope getScope() {
        return scope;
    }

    public Predicate getPrerequisite() {
        return prerequisite;
    }

    public Function getEffect() {
        return effect;
    }

    /**
     * Creates Trait that subtract hit point
     * @param damage - positive value defining how much damage inflicted to unit
     * @return - trait subtracting hit point according to damage inflicted to unit
     */
    public static Trait injury(Integer damage){
        return new Trait(Type.HIT_POINTS, Scope.UNIT, new Effects.Value(-damage.doubleValue()));
    }
}
