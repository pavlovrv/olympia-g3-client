package ru.pbem.olympia.battles.trait.conditions;

import ru.pbem.olympia.battles.Unit;
import ru.pbem.olympia.battles.trait.Type;

import java.util.function.Predicate;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 09.07.2015
 */
public class InAttackingArmy implements Predicate<Unit> {
    @Override
    public boolean test(Unit unit) {
        return unit.hasTrait(Type.IN_ATTACKING_ARMY);
    }
}
