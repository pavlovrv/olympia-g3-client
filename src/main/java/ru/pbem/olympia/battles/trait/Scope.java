package ru.pbem.olympia.battles.trait;

/**
 * Defines area of trait effect.
 * User: Роман
 * Date: 06.06.2015
 */
public enum Scope {
    STACK, // Noble, followers and other stacks under this noble
    FOLLOWERS, // Common units in inventory of noble
    UNIT, // Unit itself
}
