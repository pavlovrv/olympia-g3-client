package ru.pbem.olympia.battles.trait;

import java.util.function.Function;
import java.util.function.Predicate;

/**
 * User: Роман
 * Date: 06.06.2015
 */
public class Bonus extends Trait implements Limited {
    protected int size;

    public Bonus(int size, Type type, Scope scope, Function effect) {
        super(type, scope, effect);
        this.size = size;
    }

    public Bonus(int size, Type type, Scope scope, Predicate prerequisite, Function effect) {
        super(type, scope, prerequisite, effect);
        this.size = size;
    }

    @Override
    public int size() {
        return size;
    }
}
