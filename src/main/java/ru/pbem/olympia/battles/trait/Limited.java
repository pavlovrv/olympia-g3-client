package ru.pbem.olympia.battles.trait;

/**
 * User: Роман
 * Date: 06.06.2015
 */
public interface Limited {
    int size();
}
