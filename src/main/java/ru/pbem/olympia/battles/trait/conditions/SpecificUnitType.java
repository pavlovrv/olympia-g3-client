package ru.pbem.olympia.battles.trait.conditions;

import ru.pbem.olympia.battles.Unit;
import ru.pbem.olympia.battles.UnitType;
import java.util.function.Predicate;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 16.07.2015
 */
public class SpecificUnitType implements Predicate<Unit> {
    protected UnitType type;

    public SpecificUnitType(UnitType type) {
        this.type = type;
    }

    @Override
    public boolean test(Unit unit) {
        return unit.isType(type);
    }
}
