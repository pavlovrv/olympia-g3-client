package ru.pbem.olympia.battles.trait.conditions;

import ru.pbem.olympia.battles.Unit;

import java.util.function.Predicate;

/**
 * Stub returning false. Use it when trait is not defined for location or for weather
 * User: Roman Pavlov
 * Date: 09.07.2015
 */
public class NotApplicable implements Predicate<Unit> {
    @Override
    public boolean test(Unit unit) {
        return false;
    }
}
