package ru.pbem.olympia.battles.trait.conditions;

import ru.pbem.olympia.battles.Unit;
import ru.pbem.olympia.battles.trait.Type;

import java.util.function.Predicate;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 30.07.2015
 */
public class HaveAttack implements Predicate<Unit> {
    protected Type type;

    public HaveAttack(Type type) {
        this.type = type;
    }

    @Override
    public boolean test(Unit unit) {
        return unit.getEffect(type) > 0;
    }
}
