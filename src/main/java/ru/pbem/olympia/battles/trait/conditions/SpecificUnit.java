package ru.pbem.olympia.battles.trait.conditions;

import ru.pbem.olympia.battles.Unit;

import java.util.Arrays;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 09.07.2015
 */
public class SpecificUnit implements Predicate<Unit> {
    Set<String> codes;

    public SpecificUnit(Unit... codes) {
        this.codes = Arrays.stream(codes).map(Unit::getCode).collect(Collectors.toSet());
    }

    @Override
    public boolean test(Unit unit) {
        return codes.contains(unit.getCode());
    }
}
