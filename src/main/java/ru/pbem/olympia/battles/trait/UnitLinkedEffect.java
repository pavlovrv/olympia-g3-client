package ru.pbem.olympia.battles.trait;

import ru.pbem.olympia.battles.Unit;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 25.08.2015
 */
public abstract class UnitLinkedEffect extends PrioritizedEffect {
    protected Unit unit;

    public UnitLinkedEffect(Unit unit, int priority) {
        super(priority);
        this.unit = unit;
    }

    public void setUnit(Unit unit){
        this.unit = unit;
    }
}
