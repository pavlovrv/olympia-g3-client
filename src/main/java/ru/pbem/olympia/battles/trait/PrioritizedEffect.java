package ru.pbem.olympia.battles.trait;

import java.util.function.UnaryOperator;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 07.07.2015
 */
public abstract class PrioritizedEffect implements UnaryOperator<Double>, Comparable<PrioritizedEffect> {
    public static int ABSOLUTE_EFFECT = 1;
    public static int RELATIVE_EFFECT = 10;

    protected int priority;

    public PrioritizedEffect(int priority) {
        this.priority = priority;
    }

    @Override
    public int compareTo(PrioritizedEffect effect) {
        return Integer.compare(this.priority, effect.priority);
    }
}
