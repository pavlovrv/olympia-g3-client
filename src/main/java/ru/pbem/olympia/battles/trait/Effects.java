package ru.pbem.olympia.battles.trait;

import org.apache.commons.lang.math.IntRange;

/**
 * User: Роман
 * Date: 06.06.2015
 */
public class Effects {
    public static class Value extends PrioritizedEffect {
        protected Double value;

        public Value(Double value) {
            super(ABSOLUTE_EFFECT);
            this.value = value;
        }

        @Override
        public Double apply(Double value) {
            return this.value + value;
        }
    }

    ;

    public static class Ratio extends PrioritizedEffect {
        protected Double ratio;

        public Ratio(Double ratio) {
            super(RELATIVE_EFFECT);
            this.ratio = ratio;
        }

        @Override
        public Double apply(Double value) {
            return value * ratio;
        }
    }

    ;

    public static class Range extends PrioritizedEffect {
        protected IntRange range;

        public Range(IntRange range) {
            super(ABSOLUTE_EFFECT);
            this.range = range;
        }

        @Override
        public Double apply(Double aDouble) {
            return Math.random() * (range.getMaximumInteger() - range.getMinimumInteger()) + range.getMinimumInteger();
        }
    }
}
