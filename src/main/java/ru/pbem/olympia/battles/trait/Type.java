package ru.pbem.olympia.battles.trait;

/**
 * User: Роман
 * Date: 06.06.2015
 */
public enum Type {
    MELEE, DEFENSE, MISSILE, HIT_POINTS, DAMAGE, BEHIND
    , PERSONAL_FIGHT_TO_THE_DEATH // Personal Fight to the Death [3403]
    , FIGHT_TO_THE_DEATH // Fight to the death [1102]
    , IN_ATTACKING_ARMY // Unit is in an army which initiate attack
    , SPECIAL // TODO: SPECIAL is just a mockup
}
