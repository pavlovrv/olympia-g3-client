package ru.pbem.olympia.battles;

import ru.pbem.olympia.battles.tag.Units;
import ru.pbem.olympia.battles.trait.Effects;
import ru.pbem.olympia.battles.trait.Scope;
import ru.pbem.olympia.battles.trait.Trait;
import ru.pbem.olympia.battles.trait.Type;
import ru.pbem.olympia.battles.trait.conditions.InAttackingArmy;
import ru.pbem.olympia.battles.trait.conditions.MissileOnly;
import ru.pbem.olympia.battles.trait.conditions.SpecificUnit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * User: Роман
 * Date: 06.06.2015
 */
public class Battlefield {
    protected Location location;
    protected Weather weather;

    public Battlefield(Location location, Weather weather) {
        this.location = location;
        this.weather = weather;
    }

    public List<Trait> getTraits() {
        List<Trait> traits = new ArrayList<>();
        traits.addAll(location.getTrait());
        traits.addAll(weather.getTrait());
        return traits;
    }

    public enum Location {
        PLAIN,
        MOUNTAIN(new Trait(Type.DEFENSE, Scope.STACK, new InAttackingArmy().negate(), new Effects.Ratio(2D))),
        DESERT,
        FOREST(new Trait(Type.DEFENSE, Scope.STACK, new InAttackingArmy().negate(), new Effects.Ratio(1.5D))),
        OCEAN(new Trait(Type.DEFENSE, Scope.STACK, new SpecificUnit(Units.CAVALIER, Units.KNIGHT, Units.PALADIN), new Effects.Value(-50D))
                , new Trait(Type.MELEE, Scope.STACK, new SpecificUnit(Units.CAVALIER, Units.KNIGHT, Units.PALADIN), new Effects.Value(-50D))),
        CITY(new Trait(Type.DEFENSE, Scope.STACK, new InAttackingArmy().negate(), new Effects.Ratio(1.25D))),
        SWAMP(new Trait(Type.DEFENSE, Scope.STACK, new InAttackingArmy().negate(), new Effects.Ratio(0.75D))
                , new Trait(Type.DEFENSE, Scope.STACK, new SpecificUnit(Units.CAVALIER, Units.KNIGHT, Units.PALADIN), new Effects.Value(-50D))
                , new Trait(Type.MELEE, Scope.STACK, new SpecificUnit(Units.CAVALIER, Units.KNIGHT, Units.PALADIN), new Effects.Value(-50D))),
        SHIP(new Trait(Type.MELEE, Scope.STACK, new SpecificUnit(Units.PIRATE), new Effects.Ratio(3D))
                , new Trait(Type.DEFENSE, Scope.STACK, new SpecificUnit(Units.PIRATE), new Effects.Ratio(3D))),
        SUB_LOCATION;

        List<Trait> trait;

        Location(Trait... trait) {
            this.trait = trait == null ? Collections.emptyList() : Arrays.asList(trait);
        }

        public List<Trait> getTrait() {
            return trait;
        }
    }

    public enum Weather {
        NORMAL,
        WIND(new Trait(Type.MISSILE, Scope.STACK, new MissileOnly(), new Effects.Ratio(0.5D))),
        RAIN(new Trait(Type.MISSILE, Scope.STACK, new MissileOnly(), new Effects.Ratio(0.5D)),
                new Trait(Type.MISSILE, Scope.STACK, new SpecificUnit(Units.ARCHER, Units.HORSE_ARCHER, Units.ELITE_ARCHER), new Effects.Ratio(0D)));

        List<Trait> trait;

        Weather(Trait... trait) {
            this.trait = trait == null ? Collections.emptyList() : Arrays.asList(trait);
        }

        public List<Trait> getTrait() {
            return trait;
        }
    }

}
