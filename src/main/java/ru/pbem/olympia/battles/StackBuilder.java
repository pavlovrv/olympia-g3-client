package ru.pbem.olympia.battles;

import org.apache.commons.lang.Validate;
import ru.pbem.olympia.battles.tag.Units;
import ru.pbem.olympia.battles.trait.*;
import ru.pbem.olympia.battles.trait.conditions.SpecificUnitType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * User: Роман
 * Date: 06.06.2015
 */
public class StackBuilder {
    protected Stack stack;

    public StackBuilder() {
        newStack();
    }

    /**
     * Initialise builder with new Stack instance
     *
     * @return this object for method chaining
     */
    public StackBuilder newStack() {
        stack = new Stack();
        return this;
    }

    /**
     * Creates new stack of monsters. Unit template is used for cloning specified number of units.
     * Mosters fight in first line and until last monster standing.
     *
     * @param number   - number of monsters in stack
     * @param template - type of monsters
     * @return - stack with one monster chosen as leader and number - 1 followers
     */
    public Stack newMonsterStack(int number, Unit template) {
        stack = new Stack();
        stack.leader = template.copyOf();
        stack.leader.addTrait(new Trait(Type.BEHIND, Scope.FOLLOWERS, new Effects.Value(1D)));
        stack.leader.addTrait(new Trait(Type.FIGHT_TO_THE_DEATH, Scope.UNIT, new Effects.Value(0D)));
        addFollowers(number - 1, template.copyOf());
        return complete();
    }

    /**
     * Initialize builder with existing Stack instance
     *
     * @param stack Stack instance to be updated via builder
     * @return this object for method chaining
     */
    public StackBuilder withStack(Stack stack) {
        this.stack = stack;
        return this;
    }

    /**
     * Creates unit and sets it as leader to stack
     *
     * @param hitPoints hit points of leader
     * @param melee     melee attack rating of leader
     * @param defense   defense rating of leader
     * @param missile   missile attack rating of leader
     * @param behind    row number leader is placed with followers
     * @return this object for method chaining
     */
    public StackBuilder setLeader(int hitPoints, int melee, int defense, int missile, int behind) {
        stack.leader = buildUnit(UnitType.NOBLE.name(), UnitType.NOBLE, hitPoints, melee, defense, missile
                , new Trait(Type.DAMAGE, Scope.UNIT, new Effects.Value((double) 1))
                , new Trait(Type.BEHIND, Scope.FOLLOWERS, new Effects.Value((double) behind))
                , new Trait(Type.FIGHT_TO_THE_DEATH, Scope.UNIT, new Effects.Value(50D))
                , new Trait(Type.PERSONAL_FIGHT_TO_THE_DEATH, Scope.UNIT, new Effects.Value(100D)));
        return this;
    }

    /**
     * Adds trait to list of leader traits. Requires stack leader to be set prior invocation.
     *
     * @param trait - new trait that leader holds
     * @return this object for method chaining
     */
    public StackBuilder addLeaderTrait(Trait trait) {
        Validate.isTrue(stack.getLeader() != null, "addLeaderTrait should be invoked after setLeader");
        stack.getLeader().addTrait(trait);
        return this;
    }

    /**
     * Replace trait in list of leader traits. Requires stack leader to be set prior invocation.
     *
     * @param trait - new trait that leader holds
     * @return this object for method chaining
     */
    public StackBuilder replaceLeaderTrait(Trait trait) {
        Validate.isTrue(stack.getLeader() != null, "addLeaderTrait should be invoked after setLeader");
        stack.getLeader().replaceTrait(trait);
        return this;
    }

    /**
     * Creates units and add them to Stack as leader followers. Behind flag is set to leader's value
     *
     * @param number    number of units to be created with given characteristics
     * @param code      unit code defining template
     * @param type      unit type
     * @param hitPoints hit points of follower
     * @param melee     melee attack rating of follower
     * @param defense   defense rating of follower
     * @param missile   missile attack rating of follower
     * @return this object for method chaining
     */
    public StackBuilder addFollowers(int number, String code, UnitType type, int hitPoints, int melee, int defense, int missile) {
        Unit unit = buildUnit(code, type, hitPoints, melee, defense, missile);
        return addFollowers(number, unit);
    }

    /**
     * Creates squad for unit template and add it to Stack as leader followers.
     * Behind flag is set to leader's value
     *
     * @param number   number of units to be created with given characteristics
     * @param template unit template used to create followers
     * @return this object for method chaining
     */
    public StackBuilder addFollowers(int number, Unit template) {
        stack.getFollowers().add(new Stack.Squad(number, template.copyOf()));
        return this;
    }

    /**
     * Stack noble with followers to leader
     *
     * @param stack noble with followers
     * @return this object for method chaining
     */
    public StackBuilder addStackedNoble(Stack stack) {
        this.stack.getStacks().add(stack);
        return this;
    }

    public StackBuilder setShelter(Unit template){
        Validate.isTrue(template.isType(UnitType.STRUCTURE), "Only units with type STRUCTURE accepted");
        stack.shelter = template;
        return this;
    }

    /**
     * Returns stack built and clean builder preventing accidental change of stack in future
     *
     * @return Stack build
     */
    public Stack complete() {
        Stack returnValue = stack;
        stack = null;
        return returnValue;
    }

    public Unit buildUnit(String code, UnitType type, int hitPoints, int melee, int defense, int missile, Trait... traits) {
        List<Trait> unitTraits = new ArrayList<>();
        addTrait(hitPoints, unitTraits, Type.HIT_POINTS);
        addTrait(melee, unitTraits, Type.MELEE);
        addTrait(defense, unitTraits, Type.DEFENSE);
        addTrait(missile, unitTraits, Type.MISSILE);
        unitTraits.addAll(Arrays.asList(traits));
        return new Unit(code, type, unitTraits);
    }

    public Unit buildShelter(String code, int hitPoints, int defense, int size) {
        Unit shelter = buildUnit(code, UnitType.STRUCTURE, hitPoints, 0, defense, 0);
        shelter.addTrait(new Bonus(size, Type.DEFENSE, Scope.STACK
                // Bonus applied to units excluding structures and siege engines
                , new SpecificUnitType(UnitType.STRUCTURE).or(new SpecificUnitType(UnitType.SIEGE_ENGINE)).negate()
                , new UnitLinkedEffect(shelter ,PrioritizedEffect.ABSOLUTE_EFFECT) {
                    @Override
                    public Double apply(Double initialValue) {
                        return initialValue + unit.getEffect(Type.DEFENSE);
                    }
                }
        ));
        return shelter;
    }

    private void addTrait(int value, List<Trait> unitTraits, Type type) {
        unitTraits.add(new Trait(type, Scope.UNIT, new Effects.Value((double) value)));
    }
}
