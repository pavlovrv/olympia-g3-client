package ru.pbem.olympia.battles.tag;

import com.codahale.metrics.MetricRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.pbem.olympia.battles.*;
import ru.pbem.olympia.battles.commands.*;
import ru.pbem.olympia.battles.trait.Scope;
import ru.pbem.olympia.battles.trait.Trait;
import ru.pbem.olympia.battles.trait.Type;
import ru.pbem.olympia.battles.trait.conditions.HaveAttack;

import java.util.List;
import java.util.function.BiConsumer;

/**
 * User: Роман
 * Date: 07.06.2015
 */
public class Battle implements ru.pbem.olympia.battles.Battle {
    private static final Logger log = LoggerFactory.getLogger(Battle.class);

    @Override
    public Result fight(Stack sAttacker, Stack sDefender, Battlefield field) {
        List<Trait> global = field.getTraits();
        Army dArmy = new Army(sDefender, global);
        global.add(new Trait(Type.IN_ATTACKING_ARMY, Scope.STACK, null, null));
        Army aArmy = new Army(sAttacker, global);
        // PHASE 0. Defenders free missile attack
        log.trace("Free missile attack for defenders");
        processMissilePhase(drawUpLines(dArmy, aArmy.useShelter()), drawUpLines(aArmy, dArmy.useShelter()));
        Result result = new Result(aArmy, dArmy, checkWinner(aArmy, dArmy));
        int round = 1;
        while (result.getVictory() == null) {
            // TODO: PHASE 1. Special attacks since round 2
            // TODO: special attacks may be cast from any rear line
            // PHASE 2. Missile attacks
            result = doPhase(round, Type.MISSILE, aArmy, dArmy, this::processMissilePhase);
            if (result.getVictory() != null) {
                break;
            }
            // PHASE 3. Melee attacks
            result = doPhase(round, Type.MELEE, aArmy, dArmy, this::processMeleePhase);
            // TODO: PHASE clean up. Post round effect like peasants fleeing
            round++;
        }
        return result;
    }

    protected Result doPhase(int round, Type type, Army aArmy, Army dArmy, BiConsumer<DrawUp, DrawUp> phaseFunction) {
        log.trace("Round {}: {} phase", round, type);
        DrawUp attacker = drawUpLines(aArmy, dArmy.useShelter());
        DrawUp defender = drawUpLines(dArmy, aArmy.useShelter());
        log.trace(" Attacker army strikes", round);
        phaseFunction.accept(attacker, defender);
        log.trace(" Defender army strikes", round);
        phaseFunction.accept(defender, attacker);
        return new Result(aArmy, dArmy, checkWinner(aArmy, dArmy));
    }

    protected Victory checkWinner(Army attacker, Army defender) {
        boolean attackerDefeated = defeated(attacker);
        boolean defenderDefeated = defeated(defender);
        if (attackerDefeated && defenderDefeated) {
            return Victory.TIE;
        } else if (attackerDefeated) {
            return Victory.DEFENDER;
        } else if (defenderDefeated) {
            return Victory.ATTACKER;
        }
        return null;
    }

    protected void processMeleePhase(DrawUp attacker, DrawUp defender) {
        // 4 attackers per 1 defender are allowed if number of defenders is less than attackers, other waits next round
        int hitsLimit = defender.getFront().size() * 4;
        doHits(attacker.getFront(), Type.MELEE, hitsLimit, new Aim(Type.MELEE, defender));
    }

    protected void processMissilePhase(DrawUp attacker, DrawUp defender) {
        doHits(attacker.getBack(), Type.MISSILE, attacker.getFront().size(), new Aim(Type.MISSILE, defender));
    }

    protected void doHits(List<Unit> attackers, Type type, int hitsLimit, Aim aim) {
        attackers.stream()
                .filter(new HaveAttack(type)) // if unit does not have attack of this type it should not participate
                .sorted(new MightiestFirst().reversed()) // strongest units should hit first
                .limit(hitsLimit) // limit hits, other units should wait next round to replace fallen
                .map(aim) // each unit should select target for hit
                .forEach(new Hit()); // hit is processed
    }

    protected boolean defeated(Army army) {
        Retreat retreat = new Retreat(army.getCommander());
        army.streamUnits().forEach(retreat);
        return retreat.retreat();
    }

    protected DrawUp drawUpLines(Army army, boolean siegeRequried) {
        DrawUp drawUp = new DrawUp(army.getShelter(), new CallToDefenders(army));
        army.streamUnits()
                .filter(new StandUp())
                .filter(u-> !u.isType(UnitType.SIEGE_ENGINE) || siegeRequried) // we should exclude siege weapons if there is no structures
                .forEach(drawUp);
        return drawUp;
    }

    public static class Result {
        protected Victory victory;
        protected Army attacker;
        protected Army defender;

        public Result(Army attacker, Army defender, Victory victory) {
            this.attacker = attacker;
            this.defender = defender;
            this.victory = victory;
        }

        public Victory getVictory() {
            return victory;
        }

        public Army getAttacker() {
            return attacker;
        }

        public Army getDefender() {
            return defender;
        }
    }

    public enum Victory {
        ATTACKER, DEFENDER, TIE
    }

    public static class Stats {
        protected static final String TOTAL_COUNT = "TOTAL_COUNT";
        protected static final String runtime = "runtime";
        protected static final String POSTFIX_DEAD = " dead";

        protected Stack attacker;
        protected Stack defender;

        protected final MetricRegistry metrics = new MetricRegistry();

        public Stats(Stack attacker, Stack defender) {
            this.attacker = attacker;
            this.defender = defender;
        }

        public void register(Result result) {
            metrics.counter(TOTAL_COUNT).inc();
            metrics.counter(result.getVictory().name()).inc();
            calculateCasualties(result.attacker);
            calculateCasualties(result.defender);
        }

        protected void calculateCasualties(Army army) {
            army.streamUnits().filter(new StandUp().negate()) // leave only dead
                    .forEach(u -> metrics.counter((u.isAttacker() ? "Attacking " : "Defending ") + " " + u.getCode() + POSTFIX_DEAD).inc()); // calculate casualties
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("Attacking stack:\n").append(attacker).append("\n");
            builder.append("Defending stack:\n").append(defender).append("\n");
            builder.append(String.format("Wins: attacker=%.2f%%, tie =%.2f%%, defender=%.2f%%"
                    , getPercent(Victory.ATTACKER.name())
                    , getPercent(Victory.TIE.name())
                    , getPercent(Victory.DEFENDER.name())));
            builder.append("\nAverage casualties:");
            metrics.getCounters().entrySet().stream().filter(entry -> entry.getKey().endsWith(POSTFIX_DEAD)).forEach(entry -> {
                builder.append("\n\t").append(String.format("%.2f of %s fall dead"
                        , (double) entry.getValue().getCount() / metrics.counter(TOTAL_COUNT).getCount()
                        , entry.getKey().substring(0, entry.getKey().length() - POSTFIX_DEAD.length())));
            });
            builder.append('\n')
                    .append(String.format("%d simulations completed in %d seconds"
                                    , metrics.counter(TOTAL_COUNT).getCount()
                                    , metrics.timer(runtime).time().stop() / 1000)
                    );
            return builder.toString();
        }

        protected double getPercent(String name) {
            return (double) metrics.counter(name).getCount() / metrics.counter(TOTAL_COUNT).getCount() * 100;
        }
    }
}
