package ru.pbem.olympia.battles.tag;

import org.apache.commons.lang.math.IntRange;
import ru.pbem.olympia.battles.StackBuilder;
import ru.pbem.olympia.battles.Unit;
import ru.pbem.olympia.battles.UnitType;
import ru.pbem.olympia.battles.trait.Effects;
import ru.pbem.olympia.battles.trait.Scope;
import ru.pbem.olympia.battles.trait.Trait;
import ru.pbem.olympia.battles.trait.Type;

/**
 * User: Роман
 * Date: 06.06.2015
 */
public class Units {
    private static final StackBuilder builder = new StackBuilder();
    // TODO: Noble should take more damage
    public static final Trait UNIT_DAMAGE = new Trait(Type.DAMAGE, Scope.UNIT, new Effects.Value((double) 1));
    public static final Trait SIEGE_DAMAGE_STRUCTURE = new Trait(Type.DAMAGE, Scope.UNIT, new Effects.Range(new IntRange(5,10)));

    public static final Unit PEASANT = builder.buildUnit("PEASANT", UnitType.MEN, 1, 1, 1, 0, UNIT_DAMAGE);
    public static final Unit POSTULANT = builder.buildUnit("POSTULANT", UnitType.MEN, 1, 1, 1, 0, UNIT_DAMAGE);
    public static final Unit WORKER = builder.buildUnit("WORKER", UnitType.MEN, 1, 1, 1, 0, UNIT_DAMAGE);
    public static final Unit SAILOR = builder.buildUnit("SAILOR", UnitType.MEN, 1, 1, 1, 0, UNIT_DAMAGE);
    public static final Unit SKIRMISHER = builder.buildUnit("SKIRMISHER", UnitType.MEN, 1, 3, 3, 0, UNIT_DAMAGE);
    public static final Unit SOLDIER = builder.buildUnit("SOLDIER", UnitType.MEN, 1, 5, 5, 0, UNIT_DAMAGE);
    public static final Unit NINJA = builder.buildUnit("NINJA", UnitType.MEN, 1, 5, 5, 0, UNIT_DAMAGE);
    public static final Unit PIRATE = builder.buildUnit("PIRATE", UnitType.MEN, 1, 10, 10, 0, UNIT_DAMAGE);
    public static final Unit PIKEMAN = builder.buildUnit("PIKEMAN", UnitType.MEN, 1, 5, 20, 0, UNIT_DAMAGE);
    public static final Unit FANATIC = builder.buildUnit("FANATIC", UnitType.MEN, 1, 10, 5, 0, UNIT_DAMAGE);
    public static final Unit LT_FOOT = builder.buildUnit("LT_FOOT", UnitType.MEN, 1, 30, 30, 0, UNIT_DAMAGE);
    public static final Unit HVY_FOOT = builder.buildUnit("HVY_FOOT", UnitType.MEN, 1, 60, 60, 0, UNIT_DAMAGE);
    public static final Unit ANGEL = builder.buildUnit("ANGEL", UnitType.MEN, 1, 25, 100, 0, UNIT_DAMAGE);
    public static final Unit CAVALIER = builder.buildUnit("CAVALIER", UnitType.MEN, 1, 75, 75, 0, UNIT_DAMAGE);
    public static final Unit KNIGHT = builder.buildUnit("KNIGHT", UnitType.MEN, 1, 90, 90, 0, UNIT_DAMAGE);
    public static final Unit PALADIN = builder.buildUnit("PALADIN", UnitType.MEN, 1, 180, 180, 0, UNIT_DAMAGE);
    public static final Unit LT_XBOWMAN = builder.buildUnit("LT_XBOWMAN", UnitType.MEN, 1, 1, 1, 15, UNIT_DAMAGE);
    public static final Unit HVY_XBOWMAN = builder.buildUnit("HVY_XBOWMAN", UnitType.MEN, 1, 1, 1, 75, UNIT_DAMAGE);
    public static final Unit ARCHER = builder.buildUnit("ARCHER", UnitType.MEN, 1, 1, 1, 35, UNIT_DAMAGE);
    public static final Unit HORSE_ARCHER = builder.buildUnit("HORSE_ARCHER", UnitType.MEN, 1, 1, 20, 40, UNIT_DAMAGE);
    public static final Unit ELITE_ARCHER = builder.buildUnit("ELITE_ARCHER", UnitType.MEN, 1, 1, 1, 45, UNIT_DAMAGE);

    // TODO: moat can be added to defend castle
    // TODO: castle can be fortified
    public static final Unit CASTLE = builder.buildShelter("CASTLE", 100, 40, 500);
    public static final Unit TOWER = builder.buildShelter("TOWER", 100, 20, 100);

    public static final Unit CATAPULT = builder.buildUnit("CATAPULT", UnitType.SIEGE_ENGINE, 1, 25, 250, 25, SIEGE_DAMAGE_STRUCTURE);
    public static final Unit BATTERING_RAM = builder.buildUnit("BATTERING_RAM", UnitType.SIEGE_ENGINE, 1, 30, 250, 0, SIEGE_DAMAGE_STRUCTURE);
    // TODO: should decrease castle bonus while alive
    public static final Unit SIEGE_TOWER = builder.buildUnit("SIEGE_TOWER", UnitType.SIEGE_ENGINE, 1, 0, 250, 0);

    public static final Unit GHOST_WARRIOR = builder.buildUnit("GHOST_WARRIOR", UnitType.MEN, 1, 0, 0, 0);
    public static final Unit ELF = builder.buildUnit("ELF", UnitType.MEN, 1, 50, 50, 100, UNIT_DAMAGE);
    public static final Unit SPIRIT = builder.buildUnit("SPIRIT", UnitType.MEN, 1, 50, 50, 0, UNIT_DAMAGE);
    public static final Unit UNDEAD = builder.buildUnit("UNDEAD", UnitType.MEN, 1, 25, 1, 0, UNIT_DAMAGE);
    public static final Unit SAVAGE = builder.buildUnit("SAVAGE", UnitType.MEN, 1, 1, 1, 0, UNIT_DAMAGE);
    public static final Unit SKELETON = builder.buildUnit("SKELETON", UnitType.MEN, 1, 5, 5, 0, UNIT_DAMAGE);
    public static final Unit BARBARIAN = builder.buildUnit("BARBARIAN", UnitType.MEN, 1, 2, 1, 0, UNIT_DAMAGE);
    public static final Unit NAZGUL = builder.buildUnit("NAZGUL", UnitType.BEAST, 1, 360, 360, 0, UNIT_DAMAGE);
    public static final Unit ZOMBIE = builder.buildUnit("ZOMBIE", UnitType.MEN, 1, 15, 15, 0, UNIT_DAMAGE);
    public static final Unit WIGHT = builder.buildUnit("WIGHT", UnitType.MEN, 1, 75, 75, 0, UNIT_DAMAGE);
    public static final Unit RATSPIDER = builder.buildUnit("RATSPIDER", UnitType.MEN, 1, 5, 5, 0, UNIT_DAMAGE);
    public static final Unit CENTAUR = builder.buildUnit("CENTAUR", UnitType.BEAST, 1, 30, 30, 30, UNIT_DAMAGE);
    public static final Unit MINOTAUR = builder.buildUnit("MINOTAUR", UnitType.BEAST, 1, 30, 5, 0, UNIT_DAMAGE);
    public static final Unit GIANT_SPIDER = builder.buildUnit("GIANT_SPIDER", UnitType.BEAST, 1, 150, 100, 0, UNIT_DAMAGE);
    public static final Unit RAT = builder.buildUnit("RAT", UnitType.BEAST, 1, 3, 3, 0, UNIT_DAMAGE);
    public static final Unit LION = builder.buildUnit("LION", UnitType.BEAST, 1, 100, 100, 0, UNIT_DAMAGE);
    public static final Unit GIANT_BIRD = builder.buildUnit("GIANT_BIRD", UnitType.BEAST, 1, 300, 250, 0, UNIT_DAMAGE);
    public static final Unit GIANT_LIZARD = builder.buildUnit("GIANT_LIZARD", UnitType.BEAST, 1, 80, 80, 0, UNIT_DAMAGE);
    public static final Unit BANDIT = builder.buildUnit("BANDIT", UnitType.MEN, 1, 3, 3, 0, UNIT_DAMAGE);
    public static final Unit CHIMERA = builder.buildUnit("CHIMERA", UnitType.BEAST, 1, 250, 250, 0, UNIT_DAMAGE);
    public static final Unit HARPIE = builder.buildUnit("HARPIE", UnitType.BEAST, 1, 80, 120, 0, UNIT_DAMAGE);
    // TODO: Dragon fires 10 fireballs with attack rating 200 each 3d round
    public static final Unit DRAGON = builder.buildUnit("DRAGON", UnitType.BEAST, 1, 1500, 1500, 0, UNIT_DAMAGE);
    public static final Unit ORC = builder.buildUnit("ORC", UnitType.MEN, 1, 20, 15, 0, UNIT_DAMAGE);
    public static final Unit GORGON = builder.buildUnit("GORGON", UnitType.BEAST, 1, 10, 20, 0, UNIT_DAMAGE);
    public static final Unit WOLF = builder.buildUnit("WOLF", UnitType.BEAST, 1, 5, 5, 0, UNIT_DAMAGE);
    public static final Unit CYCLOPS = builder.buildUnit("CYCLOPS", UnitType.MEN, 1, 25, 75, 0, UNIT_DAMAGE);
    public static final Unit GIANT = builder.buildUnit("GIANT", UnitType.MEN, 1, 75, 25, 0, UNIT_DAMAGE);
    public static final Unit FAERY = builder.buildUnit("FAERY", UnitType.MEN, 1, 1, 1, 1, UNIT_DAMAGE);
    public static final Unit OTTER = builder.buildUnit("OTTER", UnitType.BEAST, 1, 10, 10, 0, UNIT_DAMAGE);
    public static final Unit MOLE = builder.buildUnit("MOLE", UnitType.BEAST, 1, 10, 10, 0, UNIT_DAMAGE);
    public static final Unit BULL = builder.buildUnit("BULL", UnitType.BEAST, 1, 10, 10, 0, UNIT_DAMAGE);
    public static final Unit EAGLE = builder.buildUnit("EAGLE", UnitType.BEAST, 1, 10, 10, 0, UNIT_DAMAGE);
    public static final Unit MONKEY = builder.buildUnit("MONKEY", UnitType.BEAST, 1, 10, 10, 0, UNIT_DAMAGE);
    public static final Unit HARE = builder.buildUnit("HARE", UnitType.BEAST, 1, 10, 10, 0, UNIT_DAMAGE);
    public static final Unit WARDOG = builder.buildUnit("WARDOG", UnitType.BEAST, 1, 10, 10, 0, UNIT_DAMAGE);
    public static final Unit SAND_RAT = builder.buildUnit("SAND_RAT", UnitType.BEAST, 1, 10, 10, 0, UNIT_DAMAGE);
    public static final Unit BALROG = builder.buildUnit("BALROG", UnitType.BEAST, 1, 1000, 1000, 0, UNIT_DAMAGE);
    public static final Unit DIRT_GOLEM = builder.buildUnit("DIRT_GOLEM", UnitType.BEAST, 1, 125, 125, 0, UNIT_DAMAGE);
    public static final Unit FLESH_GOLEM = builder.buildUnit("FLESH_GOLEM", UnitType.BEAST, 1, 250, 250, 0, UNIT_DAMAGE);
    public static final Unit IRON_GOLEM = builder.buildUnit("IRON_GOLEM", UnitType.BEAST, 1, 500, 500, 0, UNIT_DAMAGE);
    public static final Unit LESSER_DEMON = builder.buildUnit("LESSER_DEMON", UnitType.BEAST, 1, 250, 250, 0, UNIT_DAMAGE);
    public static final Unit GREATER_DEMON = builder.buildUnit("GREATER_DEMON", UnitType.BEAST, 1, 500, 500, 0, UNIT_DAMAGE);
}
