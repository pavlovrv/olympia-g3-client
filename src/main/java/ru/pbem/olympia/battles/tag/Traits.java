package ru.pbem.olympia.battles.tag;

import org.apache.commons.lang.Validate;
import ru.pbem.olympia.battles.UnitType;
import ru.pbem.olympia.battles.trait.Effects;
import ru.pbem.olympia.battles.trait.Scope;
import ru.pbem.olympia.battles.trait.Trait;
import ru.pbem.olympia.battles.trait.Type;
import ru.pbem.olympia.battles.trait.conditions.SpecificUnitType;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 15.07.2015
 */
public class Traits {
    public Trait createCombatTactics(int skillLevel) {
        Validate.isTrue(skillLevel > 0, "Skill level can't be negative");
        double bonusRatio = Math.min(2, 0.02 * skillLevel + 1);
        // TODO: this trait affect MISSILE rating too
        return new Trait(Type.MELEE, Scope.FOLLOWERS
                , new SpecificUnitType(UnitType.NOBLE).negate() // This trait is not applicable to noble itself
                , new Effects.Ratio(bonusRatio));
    }
}
