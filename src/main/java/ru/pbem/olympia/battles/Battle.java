package ru.pbem.olympia.battles;

/**
 * User: Роман
 * Date: 06.06.2015
 */
public interface Battle {
    ru.pbem.olympia.battles.tag.Battle.Result fight(Stack attacker, Stack defender, Battlefield field);
}
