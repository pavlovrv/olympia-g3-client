package ru.pbem.olympia.battles;

import ru.pbem.olympia.battles.trait.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * User: Роман
 * Date: 06.06.2015
 */
public class Unit {
    protected String code;
    protected UnitType type;
    protected List<Trait> traits;

    public Unit(String code, UnitType type, List<Trait> traits) {
        this.code = code;
        this.type = type;
        this.traits = traits;
    }

    /**
     * Clones unit by copying all traits
     */
    public Unit copyOf() {
        Unit unit = new Unit(code, type, new ArrayList<>(traits));
        for (Trait trait: unit.getTraits()){
            if (trait.getEffect() instanceof UnitLinkedEffect){
                ((UnitLinkedEffect) trait.getEffect()).setUnit(unit);
            }
        }
        return unit;
    }

    public String getCode() {
        return code;
    }

    public boolean isType(UnitType type) {
        return this.type.equals(type);
    }

    public int getBehind() {
        return getEffect(Type.BEHIND).intValue();
    }

    public Double getMelee() {
        return getEffect(Type.MELEE);
    }

    public Double getMissile() {
        return getEffect(Type.MISSILE);
    }

    public Double getDefense() {
        return getEffect(Type.DEFENSE);
    }

    public Double getHitPoints() {
        return getEffect(Type.HIT_POINTS);
    }

    public Double getDamage() {
        return getEffect(Type.DAMAGE);
    }

    public boolean isAttacker() {
        return hasTrait(Type.IN_ATTACKING_ARMY);
    }

    /**
     * Add trait to unit list of traits
     *
     * @param trait - trait to be added
     */
    public void addTrait(Trait trait) {
        this.traits.add(trait);
    }

    /**
     * Add traits to unit list of traits
     *
     * @param traits - trait to be added
     */
    public void addTraits(List<Trait> traits) {
        this.traits.addAll(traits);
    }

    /**
     * All traits of same type will be removed and trait is added
     *
     * @param trait - trait that should replace all existing traits of same type
     */
    public void replaceTrait(Trait trait) {
        traits = traits.stream().filter(t -> !t.getType().equals(trait.getType())).collect(Collectors.toList());
        addTrait(trait);
    }

    /**
     * Returns list of units traits, if scopes provided filtered list returned
     *
     * @param scopes - scopes for trait filtering, if nothing is provided full list is returned
     * @return - unmodifiable list of trait
     */
    public List<Trait> getTraits(Scope... scopes) {
        return doGetTraits(scopes).collect(Collectors.toList());
    }

    /**
     * Calculates total value for trait type. All traits of defined type are selected, effects are sorted by
     * priority and total effect value is calculated.
     *
     * @param trait - trait type to be calculated
     * @return - total value of specific trait
     */
    public Double getEffect(Type trait) {
        return (Double) getTraitByType(trait)
                .map(Trait::getEffect) // Convert traits to effects
                .sorted() // Sort effects by priority
                .reduce((e1, e2) -> new Effects.Value((Double) e2.apply(e1.apply(0D)))) // Calculate absolute effect value
                .get().apply(0D); // return absolute value
    }

    /**
     * Check whether unit has specific trait. Only occurrence of trait is checked, value is not analysed
     *
     * @param type - trait type to be checked
     * @return - true if at least one Trait object of specific type is present
     */
    public boolean hasTrait(Type type) {
        return getTraitByType(type).findFirst().isPresent();
    }

    protected Stream<Trait> doGetTraits(Scope[] scopes) {
        if (scopes == null || scopes.length == 0) {
            return traits.stream();
        }
        HashSet<Scope> sScopes = new HashSet<>(Arrays.asList(scopes));
        return traits.stream().filter(t -> sScopes.contains(t.getScope()));
    }

    /**
     * Returns traits of type applied to unit. If noble has trait of specified type but with scope FOLLOWERS,
     * it will not be returned
     * @param type - type of trait
     * @return - stream of traits of specified type
     */
    protected Stream<Trait> getTraitByType(Type type) {
        return traits.stream().filter(trait -> trait.getType().equals(type)) // filter trait by type
                .filter(t -> t.getPrerequisite() == null || t.getPrerequisite().test(this)); // check that trait is applicable
    }

    @Override
    public String toString() {
        return String.format("%s(%.0f,%.0f,%.0f)", getCode(), getMelee(), getDefense(), getMissile());
    }
}
