package ru.pbem.olympia.battles;

/**
 * User: Роман
 * Date: 06.06.2015
 */
public enum UnitType {
    NOBLE, MEN, BEAST, STRUCTURE, SIEGE_ENGINE
}
