package ru.pbem.olympia.battles.commands;

import org.apache.commons.lang.Validate;
import ru.pbem.olympia.battles.Army;
import ru.pbem.olympia.battles.Unit;
import ru.pbem.olympia.battles.UnitType;
import ru.pbem.olympia.battles.trait.Type;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 07.07.2015
 */
public class Aim implements Function<Unit, Duel> {
    protected Type phase;
    protected DrawUp enemy;

    public Aim(Type phase, DrawUp enemy) {
        this.phase = phase;
        this.enemy = enemy;
    }

    @Override
    public Duel apply(Unit unit) {
        if (unit.isType(UnitType.SIEGE_ENGINE)){
            // if siege engine then attack shelter only
            Validate.notNull(enemy.getShelter(), "Draw up procedure should not add siege engines if enemy does not use shelters");
            return new Duel(phase, unit, enemy.getShelter());
        }
        List<Unit> targets = new ArrayList<>(enemy.getFront());
        if (enemy.getShelter() != null) {
            // if shelter exist units may attack it instead soldiers
            targets.add(enemy.getShelter());
        }
        Unit target = chooseTarget(targets);
        /* From the rules: Regardless of whether they are in the front row or behind, nobles will not
        receive a hit until all the troops they personally command have been killed. */
        List<Unit> defenders = enemy.getDefenders(target);
        while(!defenders.isEmpty()) {
            targets.remove(target);
            target = chooseTarget(targets);
            defenders = enemy.getDefenders(target);
        }
        return new Duel(phase, unit, target);
    }

    protected Unit chooseTarget(List<Unit> targets) {
        int targetIndex = new Random().nextInt(targets.size());
        return targets.get(targetIndex);
    }
}
