package ru.pbem.olympia.battles.commands;

import ru.pbem.olympia.battles.Army;
import ru.pbem.olympia.battles.Unit;
import ru.pbem.olympia.battles.UnitType;
import ru.pbem.olympia.battles.trait.conditions.SpecificUnitType;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * User: Роман
 * Date: 22.08.2015
 */
public class CallToDefenders implements Function<Unit, List<Unit>> {
    protected Army army;

    public CallToDefenders(Army army) {
        this.army = army;
    }

    @Override
    public List<Unit> apply(Unit unit) {
        if (!unit.isType(UnitType.NOBLE)){
            return Collections.EMPTY_LIST; // only nobles may be protected
        }
        return army.streamFollowers(unit).filter(new StandUp()) // unit should be alive
                .filter(new SpecificUnitType(UnitType.SIEGE_ENGINE).negate()) // siege engines are not counted as protectors, see combat.c init_prot
                .collect(Collectors.toList());
    }
}
