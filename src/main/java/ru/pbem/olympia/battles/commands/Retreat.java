package ru.pbem.olympia.battles.commands;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.pbem.olympia.battles.Unit;
import ru.pbem.olympia.battles.UnitType;
import ru.pbem.olympia.battles.trait.Type;

import java.util.function.Consumer;

/**
 * Define whether army retreat or not. It calculates  ratio of alive units strength vs fallen.
 * Strength is defined as sum of unit defense + max(melee, missile).
 * Stack leader trait may affect threshold at which army retreat
 */
public class Retreat implements Consumer<Unit> {
    private static final Logger log = LoggerFactory.getLogger(Retreat.class);

    protected double all;
    protected double live;
    protected double threshold;
    protected StandUp ableToFight = new StandUp();

    /**
     * Creates Retreat validator. Type.FIGHT_TO_THE_DEATH trait of stack leader is used as army break point threshold
     *
     * @param stackLeader - stack leader with Type.FIGHT_TO_THE_DEATH trait set
     */
    public Retreat(Unit stackLeader) {
        this.threshold = stackLeader.getEffect(Type.FIGHT_TO_THE_DEATH);
    }

    /**
     * Process unit from army adding its strength to army total and alive total if unit is able to fight
     *
     * @param unit - army unit
     */
    @Override
    public void accept(Unit unit) {
        double attack = unit.getMelee();
        double missile = unit.getMissile();
        // structures and siege engines are not calculated in this limit, see combat.c -> combat_sum
        double weight = unit.isType(UnitType.SIEGE_ENGINE) || unit.isType(UnitType.STRUCTURE) ? 0 :
                unit.getDefense() + (attack > missile ? attack : missile);
        all += weight;
        live += ableToFight.test(unit) ? weight : 0;
    }

    public boolean retreat() {
        boolean retreat = live / all * 100 <= threshold;
        log.trace("Army rating live {} out of {} with threshold {}%, retreat - {}", live, all, threshold, retreat);
        return retreat;
    }
}
