package ru.pbem.olympia.battles.commands;

import ru.pbem.olympia.battles.Unit;
import ru.pbem.olympia.battles.trait.Type;

/**
 * Structure to hold attacker, defender and type of hit we process
 * User: Roman Pavlov
 * Date: 07.07.2015
 */
public class Duel {
    protected Type type;
    protected Unit attacker;
    protected Unit defender;

    public Duel(Type type, Unit attacker, Unit defender) {
        this.type = type;
        this.attacker = attacker;
        this.defender = defender;
    }

    public Type getType() {
        return type;
    }

    public Unit getAttacker() {
        return attacker;
    }

    public Unit getDefender() {
        return defender;
    }
}
