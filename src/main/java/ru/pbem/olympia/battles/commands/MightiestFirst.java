package ru.pbem.olympia.battles.commands;

import ru.pbem.olympia.battles.Unit;

import java.util.Comparator;

/**
 * Compares two unit based on theirs strength.
 * If a noble owns more men than he can control, he will use the men with the highest total attack + missile + defense
 * values, or in the case of a tie, the men with the higher attack + missile value.
 */
public class MightiestFirst implements Comparator<Unit> {

    /**
     * Compares two unit based on theirs strength.
     * @param unit1 - first unit for comparison
     * @param unit2 - second unit for comparison
     * @return - return value follows specification defined for Comparator.compare method
     */
    @Override
    public int compare(Unit unit1, Unit unit2) {
        int result = Double.compare(getFullStrength(unit1), getFullStrength(unit2));
        if (result==0){
            // If TIE check melee and missile only
            return Double.compare(getAttackStrength(unit1), getAttackStrength(unit2));
        }
        return result;
    }

    protected double getFullStrength(Unit unit) {
        return getAttackStrength(unit) + unit.getDefense();
    }

    protected double getAttackStrength(Unit unit) {
        return unit.getMelee() + unit.getMissile();
    }
}
