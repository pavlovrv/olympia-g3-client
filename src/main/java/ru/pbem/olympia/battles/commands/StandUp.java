package ru.pbem.olympia.battles.commands;

import ru.pbem.olympia.battles.Unit;
import ru.pbem.olympia.battles.UnitType;
import ru.pbem.olympia.battles.trait.Type;

import java.util.function.Predicate;

/**
 * Check whether unit is able to fight. Common unit is able to fight if health > 0, noble fights until health >= threshold set
 * User: Roman Pavlov
 * Date: 07.07.2015
 */
public class StandUp implements Predicate<Unit> {
    /**
     * Check whether unit is able to fight
     * @param unit - unit that need to be validated
     * @return - true if unit is able to fight, false otherwise
     */
    @Override
    public boolean test(Unit unit) {
        double health = unit.getEffect(Type.HIT_POINTS);
        double threshold = 1; // common unit has 1 hit point
        if (unit.isType(UnitType.NOBLE)){
            // Wounded noble retreat from battle if his health drops below threshold
            threshold = unit.getEffect(Type.PERSONAL_FIGHT_TO_THE_DEATH);
        }
        return health >= threshold;
    }
}
