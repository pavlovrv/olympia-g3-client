package ru.pbem.olympia.battles.commands;

import ru.pbem.olympia.battles.Unit;
import ru.pbem.olympia.battles.UnitType;
import ru.pbem.olympia.battles.trait.Type;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

/**
 * Form 3 unit lists for next round of battle
 * 1. Front line first army line with units
 * 2. Back line, army line with units after one put to front line
 * 3. Special units, units with ability of special attacks who may use them in special attacks phase
 */
public class DrawUp implements Consumer<Unit> {
    protected Unit shelter;
    protected List<Unit> front = new ArrayList<>();
    protected List<Unit> back = new ArrayList<>();
    protected List<Unit> special = new ArrayList<>();

    protected Unit previousUnit;
    protected boolean vulnerableRegistered;
    protected CallToDefenders defenders;
    protected List<Unit> currentLine = front;

    public DrawUp(Unit shelter, CallToDefenders defenders) {
        this.shelter = shelter;
        this.defenders = defenders;
    }

    /**
     * Put unit in appropriate line or discard it for a moment if unit too far behind front line
     * @param unit - unit of army drawing up into battle line
     */
    @Override
    public void accept(Unit unit) {
        previousUnit = previousUnit == null ? unit : previousUnit;
        // If stack leader alone in first line, second line moves to protect him, stack leader takes damage the last
        if (vulnerableRegistered && previousUnit.getBehind() != unit.getBehind()) {
            // If non noble instance is present in current line, switch line
            currentLine = back;
        }
        currentLine.add(unit);
        previousUnit = unit;
        // If only nobles in current line, check whether at least one is vulnerable
        vulnerableRegistered = vulnerableRegistered // vulnerable already registered
                || !unit.isType(UnitType.NOBLE) // followers are vulnerable
                || defenders.apply(unit).isEmpty(); // first noble without defenders

        // Select units with special abilities who will participate in SPECIAL attacks round
        // units in front line can't use special abilities
        if (unit.hasTrait(Type.SPECIAL) && !front.contains(unit)){
            special.add(unit);
        }
    }

    /**
     * Returns front line of melee fighters that face enemy and defend back lines
     * @return - list of units that face enemy
     */
    public List<Unit> getFront() {
        return Collections.unmodifiableList(front);
    }

    /**
     * Returns line behind front line that capable to send arrows to enemy
     * @return - list of units who capable to shot at enemy
     */
    public List<Unit> getBack() {
        return Collections.unmodifiableList(back);
    }

    /**
     * Returns units with special abilities. Units in front line can't use SPECIAL ability and are not returned.
     * @return - list of units with special abilities
     */
    public List<Unit> getSpecial() {
        return Collections.unmodifiableList(special);
    }

    /**
     * Returns  defenders for unit. Followers or nobles stacked immediately under noble
     * @param unit - unit that is defended
     * @return - followers or nobles stacked immediately under noble
     */
    public List<Unit> getDefenders(Unit unit) {
        return defenders.apply(unit);
    }

    public Unit getShelter() {
        return shelter;
    }
}
