package ru.pbem.olympia.battles.commands;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.pbem.olympia.battles.Unit;
import ru.pbem.olympia.battles.UnitType;
import ru.pbem.olympia.battles.trait.Effects;
import ru.pbem.olympia.battles.trait.Scope;
import ru.pbem.olympia.battles.trait.Trait;
import ru.pbem.olympia.battles.trait.Type;

import java.util.Random;
import java.util.function.Consumer;

/**
 * Resolve Duel calculating change of attacker hit defender. Once chance is calculated random number is generated and
 * chance to hit validated. If hit is successful damage is calculated and injury added to defender.
 * User: Roman Pavlov
 * Date: 07.07.2015
 */
public class Hit implements Consumer<Duel> {
    private static final Logger log = LoggerFactory.getLogger(Hit.class);
    /**
     * Resolve Duel calculating change of attacker hit defender.
     * @param duel - duel object holding attacker, defender and type of attack
     */
    @Override
    public void accept(Duel duel) {
        String baseStats = duel.getDefender().toString();
        double hitPercent = hitChance(duel);
        boolean success = rnd100() <= hitPercent;
        int damage = 0;
        if (success) {
            // TODO: blessed target may hit with 50% chance
            damage = calculateDamage(duel);
            injury(duel, damage);
        }
        log.trace("{} attacks {} with {}: {} inflicting {} damage"
                , duel.getAttacker(), baseStats, duel.getType(), success ? "HIT" : "MISS", damage);
    }

    protected void injury(Duel duel, int damage) {
        Unit defender = duel.getDefender();
        if (defender.isType(UnitType.STRUCTURE)){
            // Structures takes damage to defense first, when defense is 0 then hit points are consumed
            double defence = defender.getEffect(Type.DEFENSE);
            if (defence > 0){
                double hitToDefense = Math.min(defence, damage); // defense can't be negative, so we do actual damage or equal to defense to bring it to zero
                damage = Math.max(0, (int)(damage - hitToDefense)); // if defense consumed all damage then, damage to hit points is 0
                defender.addTrait(new Trait(Type.DEFENSE, Scope.UNIT, new Effects.Value(-hitToDefense)));
            }
        }
        defender.addTrait(Trait.injury(damage));
    }

    protected double hitChance(Duel duel) {
        // TODO: pikeman has doubled defence against cavalry, we should calculate defense based on CONTEXT trait type
        // , which precondition checks that attacker is cavalry
        double attack = duel.getAttacker().getEffect(duel.getType());
        double defense = duel.getDefender().getDefense();
        return attack / (attack + defense) * 100;
    }

    protected int calculateDamage(Duel duel) {
        Double damage = duel.getAttacker().getDamage();
        // TODO: think how to move this logic to trait as context
        return duel.getDefender().isType(UnitType.NOBLE) ? rnd100() : damage.intValue();
    }

    protected int rnd100() {
        return new Random().nextInt(100) + 1;
    }
}
