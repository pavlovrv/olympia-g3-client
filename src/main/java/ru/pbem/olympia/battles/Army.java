package ru.pbem.olympia.battles;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.Validate;
import ru.pbem.olympia.battles.trait.Limited;
import ru.pbem.olympia.battles.trait.Scope;
import ru.pbem.olympia.battles.trait.Trait;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/**
 * User: Роман
 * Date: 16.06.2015
 */
public class Army {
    protected Unit commander;
    protected Unit shelter;
    protected Map<Unit, List<Unit>> directlyCommand; // stores followers and
    protected List<List<Unit>> lines;
    protected Map<Limited, AtomicInteger> register = new HashMap<>();


    public Army(Stack stack, List<Trait> global) {
        this(stack, global, 9);
    }

    public Army(Stack stack, List<Trait> global, int size) {
        Validate.notNull(stack.getLeader(), "Stack leader is mandatory");
        global = new ArrayList<>(global); // this is to prevent accident change
        this.lines = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            lines.add(new ArrayList<>());
        }
        this.directlyCommand = new HashMap<>();
        if (stack.getShelter() != null) {
            this.shelter = stack.getShelter().copyOf();
            global.addAll(stack.getShelter().getTraits(Scope.STACK));
        }
        addStack(stack, global);
    }

    public Unit getCommander() {
        return commander;
    }

    public Unit getShelter() {
        return shelter;
    }

    public boolean useShelter(){
        return this.shelter != null;
    }

    public Stream<Unit> streamUnits() {
        // iterate through lines returns not empty as list of unit in line
        return lines.stream().filter(CollectionUtils::isNotEmpty).flatMap(Collection::stream);
    }

    /**
     * Returns stream of followers who participate in battle with this leader
     *
     * @param leader - unit leading followers
     * @return - stream of followers
     */
    public Stream<Unit> streamFollowers(Unit leader) {
        return directlyCommand.get(leader).stream();
    }

    /**
     * Parse stack and add units to army
     *
     * @param stack  - stack to be added to army
     * @param global - traits ranted by top leaders and battlefield
     * @return - created leader clone of stack added to army
     */
    protected Unit addStack(Stack stack, List<Trait> global) {
        Unit leaderClone = stack.getLeader().copyOf();
        addTraits(leaderClone, global); // Apply global traits to clone, who actually participate in battle
        if (commander == null) { // This is first stack added to army, its leader becomes commander
            commander = leaderClone;
        }
        createUnits(leaderClone, stack.getFollowers());
        for (Stack noble : stack.getStacks()) {
            Unit stackLeader = addStack(noble, leaderClone.getTraits(Scope.STACK));
            directlyCommand.get(leaderClone).add(stackLeader);
        }
        return leaderClone;
    }

    protected void createUnits(Unit leaderClone, List<Stack.Squad> squads) {
        List<Trait> global = leaderClone.getTraits(Scope.STACK, Scope.FOLLOWERS);
        List<Unit> followers = new ArrayList<>();
        for (Stack.Squad squad : squads) {
            Unit template = squad.unit.copyOf();
            for (int i = 0; i < squad.num; i++) {
                Unit unit = template.copyOf();
                addTraits(unit, global); // Apply global traits to clone, who actually participate in battle
                followers.add(unit);
            }
        }

        directlyCommand.put(leaderClone, followers);

        List<Unit> line = lines.get(leaderClone.getBehind() - 1);
        line.addAll(followers);
        line.add(leaderClone);
    }

    protected void addTraits(Unit unit, List<Trait> traits) {
        for (Trait trait : traits) {
            if (trait instanceof Limited) {
                if (hasSpace((Limited) trait)) {
                    register.get(trait).incrementAndGet();
                } else {
                    continue;
                }
            }
            unit.addTrait(trait);
        }
    }

    protected boolean hasSpace(Limited trait) {
        AtomicInteger registered = register.get(trait);
        if (registered == null) {
            registered = new AtomicInteger(0);
            register.put(trait, registered);
        }
        return registered.get() < trait.size();
    }
}
