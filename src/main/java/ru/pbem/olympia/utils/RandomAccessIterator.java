package ru.pbem.olympia.utils;

import java.util.Iterator;

/**
 * User: roman.pavlov
 * Date: 22-Sep-2010
 * Time: 12:43:54
 */
public interface RandomAccessIterator<T> extends Iterator<T> {
    boolean remove(T element);
}
