package ru.pbem.olympia.utils;

import java.util.List;

/**
 * User: Roman Pavlov
 * Date: 08-Sep-2010
 * Time: 19:12:46
 */
public interface Hierarchy<K, V> {
    boolean removeNode(K removeNode);

    void addNode(K key, V value, K addToNode);

    Hierarchy.Entry<K, V> getNode(K key);

    void moveNode(K key, K newRoot);

    interface Entry<K, V> {
        K getKey();

        V getValue();

        K getParent();

        List<K> getChildren();
    }
}
