package ru.pbem.olympia.parser.text;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/**
 * User: Roman Pavlov
 * Date: 15-Oct-2010
 * Time: 19:18:06
 */
@SuppressWarnings({"HardcodedFileSeparator"})
public enum BlockType {
    Header("^Olympia G3") //NON-NLS
    , Faction("^[ \\w]{1,35} \\[[a-z]{2}\\d\\]$") //NON-NLS
    , Noble("^[ \\w]{1,35} \\[\\d{4}\\]$") //NON-NLS
    , Region("^[A-Za-z ]+ \\[(?:[a-z]{2}\\d{2})\\],.*, (?:(?:civ-\\d)|(?:wilderness))$" //NON-NLS
            , "^[A-Za-z ]+ \\[(?:[a-z]\\d{2})\\], city, in province [A-Za-z ]+ \\[[a-z]{2}\\d{2}\\]$") //NON-NLS
    , LoreSheets("^Lore sheets$") //NON-NLS
    , NewPlayers("^New players$") //NON-NLS
    , OrderTemplate("^Order template$"); //NON-NLS

    private final List<Pattern> patterns;

    BlockType(final String... regexps) {
        this.patterns = new ArrayList<Pattern>(regexps.length);
        for (final String regexp : regexps) {
            this.patterns.add(Pattern.compile(regexp));
        }
    }

    public List<Pattern> getPatterns() {
        return Collections.unmodifiableList(this.patterns);
    }

    @Override
    public String toString() {
        return "BlockType{" +
                "patterns=" + StringUtils.join(this.patterns, ',') +
                '}';
    }
}
