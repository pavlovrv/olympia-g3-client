package ru.pbem.olympia.parser.text;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Date: 16.10.2010
 * Time: 19:29:18
 */
public class ReportBlock{
    private BlockType type;
    private List<String> lines = new ArrayList<String>();

    public ReportBlock(final BlockType type) {
        this.type = type;
    }

    public void addLine(final String line){
        this.lines.add(line);
    }

    public BlockType getType() {
        return this.type;
    }

    public List<String> getLines() {
        return Collections.unmodifiableList(this.lines);
    }

    @SuppressWarnings({"HardcodedLineSeparator"})
    @Override
    public String toString() {
        return "ReportBlock{" +
                "type=" + this.type +
                ", lines=\n" + StringUtils.join(this.lines, '\n') +
                '}';
    }
}
