package ru.pbem.olympia.parser;

/**
 * Date: 16.10.2010
 * Time: 21:26:09
 */
public class ParseException extends Exception{
    public ParseException(final String message) {
        super(message);
    }

    public ParseException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ParseException(final Throwable cause) {
        super(cause);
    }
}
