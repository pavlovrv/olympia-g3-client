package ru.pbem.olympia.parser;

import java.io.InputStream;

/**
 * User: Roman Pavlov
 * Date: 15-Oct-2010
 * Time: 18:46:30
 */
public interface ReportParser {
    void parse(InputStream in) throws ParseException;
}
