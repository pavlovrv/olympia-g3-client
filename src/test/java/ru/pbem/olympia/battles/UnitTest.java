package ru.pbem.olympia.battles;

import org.junit.Test;
import ru.pbem.olympia.battles.trait.Effects;
import ru.pbem.olympia.battles.trait.Scope;
import ru.pbem.olympia.battles.trait.Trait;
import ru.pbem.olympia.battles.trait.Type;

import static junit.framework.Assert.assertEquals;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 07.07.2015
 */
public class UnitTest {
    @Test
    public void testGetEffect() {
        Unit unit = new StackBuilder().buildUnit("noble", UnitType.NOBLE, 100, 10, 80, 0
                , new Trait(Type.DEFENSE, Scope.UNIT, new Effects.Ratio(2D))
                , new Trait(Type.MELEE, Scope.UNIT, new Effects.Ratio(2D))
                , new Trait(Type.DEFENSE, Scope.UNIT, new Effects.Ratio(2D))
                , new Trait(Type.MELEE, Scope.UNIT, new Effects.Ratio(0.2D))
                , new Trait(Type.DEFENSE, Scope.UNIT, new Effects.Value(2D))
                , new Trait(Type.MELEE, Scope.UNIT, new Effects.Value(1D))
        );
        assertEquals(4.4D, unit.getEffect(Type.MELEE));
    }
}
