package ru.pbem.olympia.battles;

import org.junit.Test;
import ru.pbem.olympia.battles.tag.Units;

/**
 * User: Роман
 * Date: 06.06.2015
 */
public class StackBuilderTest {
    @Test
    public void createStack(){
        Stack stack = new StackBuilder().newStack()
                .setLeader(100, 80, 80, 0, 1)
                .addFollowers(10, "SOLDIER", UnitType.MEN, 1, 5, 5, 0)
                .addFollowers(10, Units.SOLDIER)
                .complete();
    }
}
