package ru.pbem.olympia.battles.trait.conditions;

import org.junit.Test;
import ru.pbem.olympia.battles.StackBuilder;
import ru.pbem.olympia.battles.UnitType;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 09.07.2015
 */
public class MissileOnlyTest {
    @Test
    public void test() {
        assertFalse(new MissileOnly().test(new StackBuilder().buildUnit("noble", UnitType.NOBLE, 100, 80, 80, 0)));
        assertTrue(new MissileOnly().test(new StackBuilder().buildUnit("noble", UnitType.NOBLE, 100, 80, 80, 50)));
    }
}
