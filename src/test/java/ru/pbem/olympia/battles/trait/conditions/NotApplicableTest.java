package ru.pbem.olympia.battles.trait.conditions;

import org.junit.Test;
import ru.pbem.olympia.battles.tag.Units;

import static org.junit.Assert.assertFalse;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 09.07.2015
 */
public class NotApplicableTest {
    @Test
    public void test() {
        assertFalse(new NotApplicable().test(Units.ANGEL.copyOf()));
        assertFalse(new NotApplicable().test(Units.SOLDIER.copyOf()));
        assertFalse(new NotApplicable().test(Units.ARCHER.copyOf()));
        assertFalse(new NotApplicable().test(Units.WARDOG.copyOf()));
    }
}
