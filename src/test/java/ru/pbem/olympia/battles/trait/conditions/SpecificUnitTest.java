package ru.pbem.olympia.battles.trait.conditions;

import org.junit.Test;
import ru.pbem.olympia.battles.tag.Units;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 09.07.2015
 */
public class SpecificUnitTest {
    @Test
    public void test(){
        SpecificUnit predicate = new SpecificUnit(Units.ARCHER.copyOf(), Units.ELITE_ARCHER.copyOf());
        assertTrue(predicate.test(Units.ARCHER.copyOf()));
        assertTrue(predicate.test(Units.ELITE_ARCHER.copyOf()));
        assertFalse(predicate.test(Units.SOLDIER.copyOf()));
        assertFalse(predicate.test(Units.BANDIT.copyOf()));
    }
}
