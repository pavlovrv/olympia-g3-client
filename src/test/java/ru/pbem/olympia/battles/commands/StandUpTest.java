package ru.pbem.olympia.battles.commands;

import org.junit.Test;
import ru.pbem.olympia.battles.StackBuilder;
import ru.pbem.olympia.battles.Unit;
import ru.pbem.olympia.battles.UnitType;
import ru.pbem.olympia.battles.trait.Effects;
import ru.pbem.olympia.battles.trait.Scope;
import ru.pbem.olympia.battles.trait.Trait;
import ru.pbem.olympia.battles.trait.Type;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 08.07.2015
 */
public class StandUpTest {
    final StandUp standUp = new StandUp();

    @Test
    public void testCommonUnit(){
        Unit unit = new StackBuilder().buildUnit("SOLDIER", UnitType.MEN, 1, 80, 80, 0);
        assertTrue(standUp.test(unit));
        unit.addTrait(Trait.injury(1));
        assertFalse(standUp.test(unit));
   }

    @Test
    public void testNobleUnit(){
        Unit unit = new StackBuilder().buildUnit("NOBLE", UnitType.NOBLE, 100, 80, 80, 0);
        unit.addTrait(new Trait(Type.PERSONAL_FIGHT_TO_THE_DEATH, Scope.UNIT, new Effects.Value(100D)));
        assertTrue(standUp.test(unit));
        unit.addTrait(Trait.injury(1));
        assertFalse(standUp.test(unit)); // By default noble retreat after first wound

        unit.replaceTrait(new Trait(Type.PERSONAL_FIGHT_TO_THE_DEATH, Scope.UNIT, new Effects.Value(50D)));
        assertTrue(standUp.test(unit)); // noble should fight until his health is below 50
        unit.addTrait(Trait.injury(50));
        assertFalse(standUp.test(unit)); // Health is 49, below threshold, run away!
   }
}
