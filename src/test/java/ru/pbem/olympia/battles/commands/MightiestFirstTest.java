package ru.pbem.olympia.battles.commands;

import org.junit.Test;
import ru.pbem.olympia.battles.StackBuilder;
import ru.pbem.olympia.battles.Unit;
import ru.pbem.olympia.battles.UnitType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 08.07.2015
 */
public class MightiestFirstTest {
    @Test
    public void compare(){
        MightiestFirst comparator = new MightiestFirst();

        Unit melee = new StackBuilder().buildUnit("test1", UnitType.MEN, 1, 5, 2, 0);
        Unit archer = new StackBuilder().buildUnit("test2", UnitType.MEN, 1, 1, 2, 3);
        assertTrue(comparator.compare(melee, archer) > 0);
        assertTrue(comparator.compare(archer, melee) < 0);

        // in case of TIE, only melee and missile attacks are summed
        melee = new StackBuilder().buildUnit("test1", UnitType.MEN, 1, 10, 2, 0);
        archer = new StackBuilder().buildUnit("test2", UnitType.MEN, 1, 1, 6, 5);
        assertTrue(comparator.compare(melee, archer) > 0);
    }
}
