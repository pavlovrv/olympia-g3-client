package ru.pbem.olympia.battles.commands;

import org.junit.Test;
import ru.pbem.olympia.battles.Unit;
import ru.pbem.olympia.battles.tag.Units;
import ru.pbem.olympia.battles.trait.Type;

import java.util.Arrays;
import java.util.PrimitiveIterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 08.07.2015
 */
public class HitTest {
    @Test
    public void accept() {
        PrimitiveIterator.OfInt randoms = Arrays.stream(new int[]{99, 80}).iterator();
        Hit hit = new Hit() {
            @Override
            protected int rnd100() {
                return randoms.next();
            }
        };
        StandUp aliveValidator = new StandUp();
        Unit defender = Units.SOLDIER.copyOf();
        Duel duel = new Duel(Type.MISSILE, Units.ELITE_ARCHER.copyOf(), defender);

        // First random is 99, attacker misses and defender should survive
        hit.accept(duel);
        assertTrue(aliveValidator.test(defender));

        // First random is 80, attacker hits and defender is dead
        hit.accept(duel);
        assertFalse(aliveValidator.test(defender));
    }

    @Test
    public void hitChance() {
        Duel duel = new Duel(Type.MISSILE, Units.ELITE_ARCHER.copyOf(), Units.SOLDIER.copyOf());
        assertEquals(45D / 50D * 100, new Hit().hitChance(duel), 0.001);
        duel = new Duel(Type.MELEE, Units.ELITE_ARCHER.copyOf(), Units.SOLDIER.copyOf());
        assertEquals(1D / 6D * 100, new Hit().hitChance(duel), 0.001);
    }

    @Test
    public void calculateDamage() {
        Duel duel = new Duel(Type.MISSILE, Units.ELITE_ARCHER.copyOf(), Units.SOLDIER.copyOf());
        assertEquals(1, new Hit().calculateDamage(duel));
    }
}
