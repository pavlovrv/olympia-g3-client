package ru.pbem.olympia.battles.commands;

import org.junit.Test;
import ru.pbem.olympia.battles.StackBuilder;
import ru.pbem.olympia.battles.Unit;
import ru.pbem.olympia.battles.UnitType;
import ru.pbem.olympia.battles.trait.Effects;
import ru.pbem.olympia.battles.trait.Scope;
import ru.pbem.olympia.battles.trait.Trait;
import ru.pbem.olympia.battles.trait.Type;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 08.07.2015
 */
public class RetreatTest {
    @Test
    public void retreat() {
        Unit leader = new StackBuilder().newStack().setLeader(100, 15, 15, 0, 1).complete().getLeader();
        Unit meleeUnit = new StackBuilder().buildUnit("test", UnitType.MEN, 1, 15, 10, 0);
        Unit archerUnit = new StackBuilder().buildUnit("test", UnitType.MEN, 1, 1, 10, 15);

        // All live, all fight
        Retreat retreat = getRetreat(leader, meleeUnit, archerUnit);
        assertEquals(80, retreat.all, 0.001);
        assertEquals(80, retreat.live, 0.001);
        assertFalse(retreat.retreat());

        // Melee unit is dead, army still fights
        meleeUnit.addTrait(Trait.injury(1));
        retreat = getRetreat(leader, meleeUnit, archerUnit);
        assertEquals(80, retreat.all, 0.001);
        assertEquals(55, retreat.live, 0.001);
        assertFalse(retreat.retreat());

        // Archer is dead, retreat!
        archerUnit.addTrait(Trait.injury(1));
        retreat = getRetreat(leader, meleeUnit, archerUnit);
        assertEquals(80, retreat.all, 0.001);
        assertEquals(30, retreat.live, 0.001);
        assertTrue(retreat.retreat());

        // If leader has Fight to the death to 0, army should fight until last man dies
        leader.replaceTrait(new Trait(Type.FIGHT_TO_THE_DEATH, Scope.UNIT, new Effects.Value(0D)));
        retreat = getRetreat(leader, meleeUnit, archerUnit);
        assertEquals(80, retreat.all, 0.001);
        assertEquals(30, retreat.live, 0.001);
        assertFalse(retreat.retreat());

        // Ledaer is wounded, battle is over
        leader.addTrait(Trait.injury(10));
        retreat = getRetreat(leader, meleeUnit, archerUnit);
        assertEquals(80, retreat.all, 0.001);
        assertEquals(0, retreat.live, 0.001);
        assertTrue(retreat.retreat());
    }

    private Retreat getRetreat(Unit leader, Unit meleeUnit, Unit archerUnit) {
        Retreat retreat = new Retreat(leader);
        retreat.accept(leader);
        retreat.accept(meleeUnit);
        retreat.accept(archerUnit);
        return retreat;
    }
}
