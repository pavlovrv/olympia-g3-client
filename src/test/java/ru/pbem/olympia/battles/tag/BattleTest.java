package ru.pbem.olympia.battles.tag;

import ch.qos.logback.classic.Level;
import org.slf4j.LoggerFactory;
import ru.pbem.olympia.battles.Battlefield;
import ru.pbem.olympia.battles.Stack;
import ru.pbem.olympia.battles.StackBuilder;

/**
 * Developed as part of Olympia
 * User: Roman Pavlov
 * Date: 08.07.2015
 */
public class BattleTest {
    public static void main(String[] args) {
        StackBuilder stackBuilder = new StackBuilder();
        Stack attacker = stackBuilder.newStack()
                .setLeader(100, 80, 80, 0, 1)
                        //.replaceLeaderTrait(new Trait(Type.FIGHT_TO_THE_DEATH, Scope.UNIT, new Effects.Value(0D)))
                        //.addFollowers(10, Units.ANGEL)
                        //.addFollowers(1, Units.BATTERING_RAM)
                /*.addStackedNoble(
                        new StackBuilder().newStack()
                                .setLeader(100, 80, 80, 50, 3)
                                .addFollowers(7, Units.HVY_XBOWMAN)
                                        //.addFollowers(1, Units.CATAPULT)
                                .complete())*/
                .addFollowers(10, Units.PEASANT)
                .addFollowers(20, Units.SOLDIER)
                .addStackedNoble(
                        new StackBuilder().newStack()
                                .setLeader(100, 80, 80, 50, 1)
                                .addFollowers(10, Units.PEASANT)
                                .addFollowers(20, Units.SOLDIER)
                                .addStackedNoble(
                                        new StackBuilder().newStack()
                                                .setLeader(100, 80, 80, 50, 1)
                                                .addFollowers(10, Units.PEASANT)
                                                .addFollowers(20, Units.SOLDIER)
                                                .addStackedNoble(
                                                        new StackBuilder().newStack()
                                                                .setLeader(100, 80, 80, 50, 1)
                                                                .addFollowers(10, Units.PEASANT)
                                                                .addFollowers(10, Units.SOLDIER)
                                                                .addFollowers(10, Units.PIKEMAN)
                                                                .addStackedNoble(
                                                                        new StackBuilder().newStack()
                                                                                .setLeader(100, 80, 80, 50, 1)
                                                                                .addFollowers(10, Units.PEASANT)
                                                                                .addFollowers(20, Units.SKIRMISHER)
                                                                                .complete())
                                                                .complete())
                                                .complete())
                                .complete())
                .complete();
        Stack defender = stackBuilder.newStack()
                .setLeader(100, 80, 80, 0, 1)
                //.setShelter(Units.CASTLE)
                        //.replaceLeaderTrait(new Trait(Type.FIGHT_TO_THE_DEATH, Scope.UNIT, new Effects.Value(0D)))
                .addFollowers(15, Units.ANGEL)
                .addStackedNoble(
                        new StackBuilder().newStack()
                                .setLeader(100, 80, 80, 50, 1)
                                .addFollowers(15, Units.ANGEL)
                                .complete())
                .complete();
        //Stack defender = stackBuilder.newMonsterStack(6, Units.GIANT_SPIDER);
        Battlefield battlefield = new Battlefield(Battlefield.Location.PLAIN, Battlefield.Weather.NORMAL);
        simulate(1000, attacker, defender, battlefield);
    }

    protected static void simulate(int num, Stack attacker, Stack defender, Battlefield battlefield) {
        Battle.Stats stats = new Battle.Stats(attacker, defender);
        if (num == 1) {
            ((ch.qos.logback.classic.Logger) LoggerFactory.getLogger("ROOT")).setLevel(Level.TRACE);
        }
        for (int i = 0; i < num; i++) {
            stats.register(new Battle().fight(attacker, defender, battlefield));
        }
        System.out.println(stats);
    }
}
