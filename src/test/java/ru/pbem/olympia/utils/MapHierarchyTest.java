package ru.pbem.olympia.utils;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * User: Roman Pavlov
 * Date: 16-Sep-2010
 * Time: 18:05:46
 */

@SuppressWarnings({"ALL"})
public class MapHierarchyTest{
    MapHierarchy<String, Long> hierarchy;

    @Before
    public void setUp(){
        this.hierarchy = new MapHierarchy<String, Long>();
        hierarchy.addNode("k1", 21L, null);
        hierarchy.addNode("k2", 22L, null);
        hierarchy.addNode("k3", 24L, "k1");
        hierarchy.addNode("k4", 25L, "k1");
        hierarchy.addNode("k5", 26L, "k3");
        hierarchy.addNode("k6", 27L, "k4");
        hierarchy.addNode("k7", 28L, "k2");
    }

    @Test
    public void testAddNode(){
        // Successful
        assertNull(hierarchy.getNode("k23"));
        hierarchy.addNode("k23", 543L, "k3");
    }

    @Test
    public void testGetNode(){

    }

    @Test
    public void testMoveNode(){

    }

    @Test
    public void testRemoveNode(){
       
    }
}
